`ckage PairProgramming;

import java.util.Random;


public class IntNumber {
    private int A;

    public int getA(){
        return A;
    }

    public IntNumber(int A){
        this.A = A;
    }

    public int add(IntNumber op1){
        int sum = A + op1.A;
        System.out.print(A + " + " + op1.A + "=");
        return sum;
    }

    public int subtract(IntNumber op1){
        int num = A - op1.A;
        System.out.print(A + " - " + op1.A + "=");
        return num;
    }

    public int multiply(IntNumber op1){
        int num = A * op1.A;
        System.out.print(A + " * " + op1.A + "=");
        return num;
    }

    public static IntNumber obj(){
        Random ran = new Random();
        return new IntNumber(ran.nextInt(20)-10);
    }

    public String divide(Score op1){
        System.out.print(op1.getNumerator() + " / " + op1.getDenominator() + "=");
       return op1.toString();
    }
}

