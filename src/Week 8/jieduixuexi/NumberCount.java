package PairProgramming;

import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.*;

public class NumberCount {
    public static void main(String[] args) {
        NumberFormat nf = NumberFormat.getPercentInstance();
        IntNumber in1, in2;
        Score score1, score2,num2;
        int num,N;
        String Q,num1;

        Random ran = new Random();
        Scanner scan = new Scanner(System.in);

        System.out.print("请输入您所需要的题目数量：");
        int A = scan.nextInt();

        for (int i = 0; i < A; i++) {
            int B = ran.nextInt(2);
            int C = ran.nextInt(4);

            in1 = IntNumber.obj();
            in2 = IntNumber.obj();
            score1 = Score.obj();
            score2 = Score.obj();
            if (B == 0) {
                switch (C) {
                    case 0:
                        num = in1.add(in2);
                        num1 = "" + num;
                        N = scan.nextInt();
                        CorrectJudgment.judgment(N == num,num1);
                        break;
                    case 1:
                        num = in1.subtract(in2);
                        num1 = "" + num;
                        N = scan.nextInt();
                        CorrectJudgment.judgment(N == num,num1);
                        break;
                    case 2:
                        num = in1.multiply(in2);
                        num1 = "" + num;
                        N = scan.nextInt();
                        CorrectJudgment.judgment(N == num,num1);
                        break;
                    case 3:
                        num1 = in1.divide(score1);
                        Q = scan.next();
                        CorrectJudgment.judgment(Q.equals(num1),num1);
                        break;
                }
            } else {
                switch (C) {
                    case 0:
                        num2 = score1.add(score2);
                        num1 = num2.toString();
                        Q = scan.next();
                        CorrectJudgment.judgment(Q.equals(num1),num1);
                        break;
                    case 1:
                        num2 = score1.subtract(score2);
                        num1 = num2.toString();
                        Q = scan.next();
                        CorrectJudgment.judgment(Q.equals(num1),num1);
                        break;
                    case 2:
                        num2 = score1.multiply(score2);
                        num1 = num2.toString();
                        Q = scan.next();
                        CorrectJudgment.judgment(Q.equals(num1),num1);
                        break;
                    case 3:
                        num2 = score1.divide(score2);
                        num1 = num2.toString();
                        Q = scan.next();
                        CorrectJudgment.judgment(Q.equals(num1),num1);
                        break;
                }
            }
        }
        System.out.println("你答对的题目总数：" + CorrectJudgment.getTrues());
        double T = (double) CorrectJudgment.getTrues()/A;
        System.out.println("您的正确率为:" + nf.format(T));
    }
}

