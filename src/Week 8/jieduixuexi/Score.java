package PairProgramming;


import java.util.Random;

public class Score {
        private int numerator, denominator;


        public  Score (int numer, int denom)
        {
            if(denom == 0 )
                denom = 1;

            if (denom < 0)
            {
                numer = numer * -1;
                denom = denom * -1;
            }

            numerator = numer;
            denominator = denom;

            reduce();
        }

        public int getNumerator()
        {
            return numerator;
        }

        public int getDenominator()
        {
            return denominator;
        }

        public Score reciprocal()
        {
            return new Score (denominator, numerator);
        }

        public Score add(Score op2)
        {
            int commonDenominator = denominator * op2.getDenominator();
            int numerator1 = numerator * op2.getDenominator();
            int numerator2 = op2.getNumerator() * denominator;
            int sum = numerator1 + numerator2;
            System.out.print("("+this.toString()+")" + " + " + "("+op2.toString()+")" + "=");
            return new Score (sum, commonDenominator);
        }

        public Score subtract(Score op2)
        {
            int commonDenominator = denominator * op2.getDenominator();
            int numerator1 = numerator * op2.getDenominator();
            int numerator2 = op2.getNumerator() * denominator;
            int difference = numerator1 - numerator2;
            System.out.print("("+this.toString()+")" + " - " + "("+op2.toString()+")" + "=");
            return new Score(difference,commonDenominator);
        }

        public Score multiply (Score op2)
        {
            int numer = numerator * op2.getNumerator();
            int denom = denominator * op2.getDenominator();
            System.out.print("("+this.toString()+")" + " * " + "("+op2.toString()+")" + "=");
            return new Score (numer, denom);
        }

        public Score divide (Score op2)
        {
            Score op1 = op2.reciprocal();
            int numer = numerator * op1.getNumerator();
            int denom = denominator * op1.getDenominator();
            System.out.print("("+this.toString()+")" + " / " + "("+op2.toString()+")" + "=");
            return new Score (numer, denom);
        }

        public boolean isLike (Score op2)
        {
            return (numerator == op2.getNumerator() &&
                    denominator == op2.getDenominator());
        }

        public String toString()
        {
            String result;

            if (numerator == 0)
                result = "0";
            else
            if (denominator == 1)
                result = numerator + "";
            else
                result = numerator + "/" + denominator;

            return result;
        }

        private void reduce()
        {
            if (numerator != 0)
            {
                int common = gcd (Math.abs(numerator), denominator);

                numerator = numerator / common;
                denominator = denominator / common;
            }
        }

        private int gcd (int num1, int num2)
        {
            while (num1 != num2)
                if (num1 > num2)
                    num1 = num1 - num2;
                else
                    num2 = num2 - num1;

            return num1;
        }

        public static Score obj(){
            Random ran = new Random();
            return new Score(ran.nextInt(20)-10,ran.nextInt(20)-10);
        }
    }

