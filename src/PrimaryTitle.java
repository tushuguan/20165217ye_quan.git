import java.util.*;

public class PrimaryTitle {
    private Random random1, random2, random3;
    private int num, s1, s2;
    private List<String> list = new ArrayList<String>();
    private List<String> lis = new ArrayList<>();

    public void show(Object c) {
        List<String> l = new ArrayList<String>();
        l = (List<String>) c;
        for (int i = 0; i < l.size(); i++) {
            System.out.println(i+1 + "." + l.get(i) + "=");
        }
    }

    // 题目数量与显示题目(无括号)n：题目数量，n1：题目长度（指定范围），n2：题目中的数字大小，n3：符号)
    public Object title1(int n, int n1, int n2, int n3, int n4) {
        int num = n;
        for (int i = 0; i < n; i++) {
            list.add(promber1(n1,n2,n3,n4));
        }
        return list;
    }

    //题目数量与显示题目(有单括号)n：题目数量，n1：题目长度（指定范围），n2：题目中的数字大小，n3：符号)
    public Object title2(int n, int n1, int n2, int n3, int n4) {
        int num = n;
        for (int i = 0; i < n; i++) {
            list.add(promber3(n1, n2, n3,n4));
        }
        return list;
    }

    //题目数量（随机生成含有括号的题目）n：题目数量，n1：题目长度（指定范围），n2：题目中的数字大小，n3：符号)
    public Object title3(int n, int n1, int n2, int n3,int n4) {
        int num = n;
        for (int i = 0; i < n; i++) {
            Random r = new Random();
            int s = r.nextInt(2) + 1;
            if (s == 1)
                list.add(promber1(n1,n2,n3,n4));
            else
                list.add(promber3(n1,n2,n3,n4));
        }
        return list;
    }

    //储存题目
    public String storage() {
        String[] str = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            str[i] = list.get(i);
        }
        //将String数组转成String类型
        String c = " ";
        for (int i = 0; i < str.length; i++) {
            c += (str[i] + "!");
        }
        return c;
    }

    // 随机符号
    public String symbol(int n2, int n1) {
        random1 = new Random();
        int num1 = random1.nextInt(n2) + n1;
        String s = " ";
        if (num1 == 1)
            s = "+";
        else if (num1 == 2)
            s = "-";
        else if (num1 == 3)
            s = "*";
        else if (num1 == 4)
            s = "/";
        return s;
    }

    // 随机数（数字范围）
    public String number(int n) {
        random2 = new Random();
        int num2 = random2.nextInt(n) + 1;
        String s = String.valueOf(num2);
        return s;
    }

    // 题目长度
    public int lenth(int n) {
        int s = n;
        return s;
    }

    //生成无括号的题目（题目长度n1：指定长度，题目数字大小n2：指定范围内,符号：n3）
    public String promber1(int n1, int n2, int n3, int n) {
        List<String> list = new ArrayList<String>();
        list.add(number(n2));
        for (int i = 1; i < lenth(n1); i++) {
            list.add(symbol(n3,n));
            list.add(number(n2));
        }
        String[] str = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            str[i] = list.get(i);
        }
        String c = " ";
        for (int i = 0; i < str.length; i++) {
            c += (str[i] + " ");
        }
        return c;
    }

    //生成有括号的题目（存在bug）
    public String promber2(int n1, int n2, int n3, int n) {
        List<String> list = new ArrayList<String>();
        String[] strings;
        do {
            list.add(number(n2));
            for (int i = 1; i < lenth(n1); i++) {
                list.add(symbol(n3,n));
                list.add(number(n2));
            }
            do {
                Random r1 = new Random();
                s1 = r1.nextInt(list.size() - 3);
            }
            while (s1 % 2 != 0);
            int s = list.size();
            do {
                Random r2 = new Random();
                s2 = r2.nextInt(s) + (s1 + 4);
            }
            while (s2 % 2 != 0 || s2 > s);
            list.add(s1, "(");
            list.add(s2, ")");
            String[] str = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                str[i] = list.get(i);
            }
            strings = str;
        }
        while ((strings[0].equals("(") && strings[strings.length - 1].equals(")")));
        lis = list.subList(s1, s2);
        String c = " ";
        for (int i = 0; i < strings.length; i++) {
            c += (strings[i] + " ");
        }
        return c;
    }

    //生成you括号的题目（题目长度n1：指定长度，题目数字大小n2：指定范围内,符号：n3）
    public String promber3(int n1, int n2, int n3, int n){
        String s;
        do {
            s = promber2(n1,n2,n3,n);
        }
        while (!isOperation1(s));
        return s;
    }

    //判断括号内的符号
    public boolean isOperation1(Object c) {
        return (lis.contains("-") || lis.contains("+"));
    }

    public boolean isOperation2(Object c) {
        return (lis.contains("+") && (lis.contains("*") || lis.contains("/")));
    }
}

